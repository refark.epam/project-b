package com.example.projectb.dto;

import lombok.Value;

/**
 * Student.
 */
@Value
public class Student {
	String name;
	Integer grade;
}
