package com.example.projectb.controller;

import com.example.projectb.dto.Student;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class StudentController {
	@GetMapping("student")
	public Mono<Student> student() {
		return Mono.just(new Student("Ilya", 11));
	}
}
